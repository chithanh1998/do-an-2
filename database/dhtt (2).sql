-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2020 at 04:13 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dhtt`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `hoten` text NOT NULL,
  `ngaysinh` date NOT NULL,
  `diachi` text NOT NULL,
  `email` text NOT NULL,
  `sdt` varchar(20) NOT NULL,
  `idtk` int(11) NOT NULL,
  `gioitinh` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `hoten`, `ngaysinh`, `diachi`, `email`, `sdt`, `idtk`, `gioitinh`) VALUES
(1, 'admin', '2020-03-12', 'Nguyen Trai', 'admin@gmail.com', '333356014', 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `baihoc`
--

CREATE TABLE `baihoc` (
  `idbh` int(11) NOT NULL,
  `tenbaihoc` varchar(50) NOT NULL,
  `noidung` text NOT NULL,
  `ngaytao` date NOT NULL,
  `ngayketthuc` date NOT NULL,
  `idlop` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `baihoc`
--

INSERT INTO `baihoc` (`idbh`, `tenbaihoc`, `noidung`, `ngaytao`, `ngayketthuc`, `idlop`) VALUES
(2, 'Bài 1', '<h2>Title</h2><p>content</p>', '2020-03-01', '2020-03-28', 2),
(11, 'Bài 2', '<p><img alt=\"\" src=\"http://localhost:8000/upload/images/FB_IMG_1583296631079.jpg\" style=\"height:615px; width:598px\" /></p>', '2020-03-19', '2020-03-27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `binhluan`
--

CREATE TABLE `binhluan` (
  `idbl` int(11) NOT NULL,
  `noidung` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `idtk` int(11) NOT NULL,
  `idlop` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `binhluan`
--

INSERT INTO `binhluan` (`idbl`, `noidung`, `created_at`, `updated_at`, `idtk`, `idlop`) VALUES
(1, 'test', '2020-03-25 11:28:43', '2020-03-25 11:28:43', 4, 1),
(3, 'a', '2020-03-26 10:03:04', '2020-03-26 10:03:04', 4, 1),
(4, 'dsg', '2020-03-26 16:01:13', '2020-03-26 16:01:13', 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `danhgia`
--

CREATE TABLE `danhgia` (
  `id` int(11) NOT NULL,
  `idtk` int(11) NOT NULL,
  `danhgia` int(11) NOT NULL,
  `idlop` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `danhgia`
--

INSERT INTO `danhgia` (`id`, `idtk`, `danhgia`, `idlop`) VALUES
(1, 10, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `giaovien`
--

CREATE TABLE `giaovien` (
  `idgv` int(11) NOT NULL,
  `hoten` varchar(50) NOT NULL,
  `ngaysinh` date NOT NULL,
  `diachi` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `sdt` varchar(20) NOT NULL,
  `idtk` int(11) NOT NULL,
  `gioitinh` tinyint(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `giaovien`
--

INSERT INTO `giaovien` (`idgv`, `hoten`, `ngaysinh`, `diachi`, `email`, `sdt`, `idtk`, `gioitinh`, `created_at`, `updated_at`) VALUES
(1, 'giaovien1', '2020-03-12', 'Nguyen Trai', 'admin@gmail.com', '333356014', 5, 1, '2020-03-24 04:26:40', '2020-03-24 07:29:35'),
(2, 'giaovien2', '2020-03-19', 'test', 'admin@gmail.com', '1111111111', 11, 2, '2020-03-24 10:49:53', '2020-03-24 10:49:53');

-- --------------------------------------------------------

--
-- Table structure for table `lop`
--

CREATE TABLE `lop` (
  `idlop` int(11) NOT NULL,
  `tenlop` varchar(50) NOT NULL,
  `idgv` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lop`
--

INSERT INTO `lop` (`idlop`, `tenlop`, `idgv`, `created_at`, `updated_at`) VALUES
(1, 'A1', 1, NULL, '2020-03-24 11:00:55'),
(2, 'B', 2, '2020-03-24 10:50:16', '2020-03-24 10:50:16');

-- --------------------------------------------------------

--
-- Table structure for table `lop-sinhvien`
--

CREATE TABLE `lop-sinhvien` (
  `id` int(11) NOT NULL,
  `idlop` int(11) NOT NULL,
  `idsv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lop-sinhvien`
--

INSERT INTO `lop-sinhvien` (`id`, `idlop`, `idsv`) VALUES
(4, 2, 3),
(5, 2, 4),
(6, 1, 3),
(7, 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `nopbaitap`
--

CREATE TABLE `nopbaitap` (
  `idbt` int(11) NOT NULL,
  `tieude` text COLLATE utf8_unicode_ci NOT NULL,
  `idsv` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `file` text COLLATE utf8_unicode_ci NOT NULL,
  `idbh` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nopbaitap`
--

INSERT INTO `nopbaitap` (`idbt`, `tieude`, `idsv`, `created_at`, `updated_at`, `file`, `idbh`) VALUES
(4, 'Test', 3, '2020-03-31 17:00:46', '2020-03-31 17:00:46', 'Danh-sach-cac-lop-hoc-phan-online.docx', 2),
(5, 'Test 2', 4, '2020-03-31 17:03:20', '2020-03-31 17:03:20', 'Đinh-Đức-Hải-Phần-mềm-dạy-học-ngoại-ngữ-trực-tuyến.docx', 2),
(6, 'Test', 4, '2020-03-31 17:09:05', '2020-03-31 17:09:05', '11-2019-D15_DS diemthanhphanguiSV_InternetvaGiaothuc.xlsx', 11);

-- --------------------------------------------------------

--
-- Table structure for table `sinhvien`
--

CREATE TABLE `sinhvien` (
  `idsv` int(11) NOT NULL,
  `hoten` varchar(50) NOT NULL,
  `ngaysinh` date NOT NULL,
  `gioitinh` tinyint(4) NOT NULL COMMENT '1: Nam 2: Nữ',
  `diachi` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `sdt` varchar(20) NOT NULL,
  `idtk` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sinhvien`
--

INSERT INTO `sinhvien` (`idsv`, `hoten`, `ngaysinh`, `gioitinh`, `diachi`, `email`, `sdt`, `idtk`, `created_at`, `updated_at`) VALUES
(3, 'sinhvien', '2020-03-04', 2, 'Nguyen Trai', 'ab@mvn.com', '1111111111', 13, '2020-03-31 16:50:53', '2020-03-31 16:50:53'),
(4, 'sinhvien2', '2020-03-10', 1, 'Nguyen Trai', 'ab@mvn.com', '0333356014', 14, '2020-03-31 17:02:05', '2020-03-31 17:02:05');

-- --------------------------------------------------------

--
-- Table structure for table `taikhoan`
--

CREATE TABLE `taikhoan` (
  `idtk` int(11) NOT NULL,
  `tentk` varchar(50) NOT NULL,
  `matkhau` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taikhoan`
--

INSERT INTO `taikhoan` (`idtk`, `tentk`, `matkhau`, `created_at`, `updated_at`) VALUES
(4, 'admin', '21232f297a57a5a743894a0e4a801fc3', '2020-03-24 04:25:12', '2020-03-24 04:25:12'),
(5, 'giaovien', '9fe9893ade1384ab1d48f52ab201723b', '2020-03-24 04:26:40', '2020-03-24 04:26:40'),
(11, 'giaovien2', '9fe9893ade1384ab1d48f52ab201723b', '2020-03-24 10:49:53', '2020-03-24 10:49:53'),
(13, 'sinhvien', '615ad206666f8086103305b8f77173f4', '2020-03-31 16:50:53', '2020-03-31 16:50:53'),
(14, 'sinhvien2', '615ad206666f8086103305b8f77173f4', '2020-03-31 17:02:05', '2020-03-31 17:02:05');

-- --------------------------------------------------------

--
-- Table structure for table `tailieu`
--

CREATE TABLE `tailieu` (
  `idtl` int(11) NOT NULL,
  `duongdan` text NOT NULL,
  `idbh` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tailieu`
--

INSERT INTO `tailieu` (`idtl`, `duongdan`, `idbh`, `created_at`, `updated_at`) VALUES
(4, 'dhtt.sql', 4, '2020-03-25 10:56:51', '2020-03-25 10:56:51');

-- --------------------------------------------------------

--
-- Table structure for table `tintuc`
--

CREATE TABLE `tintuc` (
  `idtt` int(11) NOT NULL,
  `tieude` varchar(200) NOT NULL,
  `noidung` text NOT NULL,
  `hinhnho` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tintuc`
--

INSERT INTO `tintuc` (`idtt`, `tieude`, `noidung`, `hinhnho`, `created_at`, `updated_at`) VALUES
(2, 'Test', '<p>test</p>', '_FB_IMG_1583296631079.jpg', '2020-03-26 09:57:02', '2020-03-26 09:57:02'),
(3, 'Test 2', '<p>jnktuyhd</p>', '_unnamed.jpg', '2020-03-26 10:03:46', '2020-03-26 10:03:46'),
(4, 'àgg', '<p><img alt=\"\" src=\"http://localhost:8000/upload/images/32fe63ef0d5eeb00b.png\" style=\"height:1000px; width:1000px\" /></p>', NULL, '2020-03-31 11:10:43', '2020-03-31 11:10:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `baihoc`
--
ALTER TABLE `baihoc`
  ADD PRIMARY KEY (`idbh`);

--
-- Indexes for table `binhluan`
--
ALTER TABLE `binhluan`
  ADD PRIMARY KEY (`idbl`);

--
-- Indexes for table `danhgia`
--
ALTER TABLE `danhgia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `giaovien`
--
ALTER TABLE `giaovien`
  ADD PRIMARY KEY (`idgv`);

--
-- Indexes for table `lop`
--
ALTER TABLE `lop`
  ADD PRIMARY KEY (`idlop`);

--
-- Indexes for table `lop-sinhvien`
--
ALTER TABLE `lop-sinhvien`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nopbaitap`
--
ALTER TABLE `nopbaitap`
  ADD PRIMARY KEY (`idbt`);

--
-- Indexes for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD PRIMARY KEY (`idsv`);

--
-- Indexes for table `taikhoan`
--
ALTER TABLE `taikhoan`
  ADD PRIMARY KEY (`idtk`);

--
-- Indexes for table `tailieu`
--
ALTER TABLE `tailieu`
  ADD PRIMARY KEY (`idtl`);

--
-- Indexes for table `tintuc`
--
ALTER TABLE `tintuc`
  ADD PRIMARY KEY (`idtt`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `baihoc`
--
ALTER TABLE `baihoc`
  MODIFY `idbh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `binhluan`
--
ALTER TABLE `binhluan`
  MODIFY `idbl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `danhgia`
--
ALTER TABLE `danhgia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `giaovien`
--
ALTER TABLE `giaovien`
  MODIFY `idgv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lop`
--
ALTER TABLE `lop`
  MODIFY `idlop` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lop-sinhvien`
--
ALTER TABLE `lop-sinhvien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `nopbaitap`
--
ALTER TABLE `nopbaitap`
  MODIFY `idbt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sinhvien`
--
ALTER TABLE `sinhvien`
  MODIFY `idsv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `taikhoan`
--
ALTER TABLE `taikhoan`
  MODIFY `idtk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tailieu`
--
ALTER TABLE `tailieu`
  MODIFY `idtl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tintuc`
--
ALTER TABLE `tintuc`
  MODIFY `idtt` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
