<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('dangnhap', 'NguoidungController@post_dangnhap');
Route::get('dangxuat', 'NguoidungController@dangxuat');
Route::get('/', 'NguoidungController@get_dangnhap');
Route::get('timkiem', 'TimkiemController@timkiem');

    Route::group(['prefix' => 'nguoidung','middleware'=>'login'], function () {
        Route::get('/', 'NguoidungController@ds')->middleware('admin');
        Route::post('them', 'NguoidungController@post_them')->middleware('admin');
        Route::get('them', 'NguoidungController@get_them')->middleware('admin');
        Route::get('sua/{id}', 'NguoidungController@get_sua')->middleware('admin');
        Route::post('sua/{id}', 'NguoidungController@post_sua')->middleware('admin');
        Route::get('xoa/{id}', 'NguoidungController@get_xoa')->middleware('admin');
        Route::get('ttcn', 'NguoidungController@get_ttcn');
        Route::post('ttcn', 'NguoidungController@post_ttcn');
        Route::get('ttcn/doimatkhau','NguoidungController@get_doimk');
        Route::post('ttcn/doimatkhau','NguoidungController@post_doimk');
    });
    Route::group(['prefix' => 'lop','middleware'=>'login'], function () {
        Route::get('/', 'LopController@ds');
        Route::post('themlop', 'LopController@themlop')->middleware('admin');
        Route::get('/{id_lop}/sualop', 'LopController@get_sualop')->middleware('admin');
        Route::post('/{id_lop}/sualop', 'LopController@post_sualop')->middleware('admin');
        Route::get('/{id_lop}/xoalop', 'LopController@xoalop')->middleware('admin');
        Route::post('/{id_lop}/sinhvien/them', 'LopController@themsv')->middleware('giaovien');
        Route::get('/{id_lop}/sinhvien', 'LopController@dssv');
        Route::get('/{id_lop}/sinhvien/xoa/{id_sv}', 'LopController@xoasv')->middleware('giaovien');
        Route::get('/{id_lop}/baihoc', 'LopController@dsbaihoc');
        Route::get('/{id_lop}/baihoc/them', 'LopController@get_thembaihoc')->middleware('giaovien');
        Route::post('/{id_lop}/baihoc/them', 'LopController@post_thembaihoc')->middleware('giaovien');
        Route::get('/{id_lop}/baihoc/xem/{id_baihoc}', 'LopController@xembaihoc');
        Route::get('/{id_lop}/baihoc/sua/{id_baihoc}', 'LopController@get_suabaihoc')->middleware('giaovien');
        Route::post('/{id_lop}/baihoc/sua/{id_baihoc}', 'LopController@post_suabaihoc')->middleware('giaovien');
        Route::get('/{id_lop}/baihoc/xoa/{id_baihoc}', 'LopController@xoabaihoc')->middleware('giaovien');
        Route::get('/{id_lop}/baihoc/{id_baihoc}/dsnopbaitap', 'LopController@dsnopbaitap')->middleware('giaovien');
        Route::get('/{id_lop}/baihoc/{id_baihoc}/nopbaitap', 'LopController@get_nopbaitap');
        Route::post('/{id_lop}/baihoc/{id_baihoc}/nopbaitap', 'LopController@post_nopbaitap');
        Route::get('/{id_lop}/binhluan', 'LopController@dsbinhluan');
        Route::post('/{id_lop}/binhluan/them', 'LopController@thembinhluan');
        Route::get('/binhluan/xoa/{id_bl}', 'LopController@xoabinhluan');
        Route::get('/xoatailieu/{id_tl}', 'LopController@xoatailieu')->middleware('giaovien');
        Route::get('/xoabaitap/{id_bt}', 'LopController@xoabaitap');
        Route::get('/layid', 'LopController@layid');
        Route::post('/danhgia', 'LopController@danhgia');


    });
    Route::group(['prefix' => 'tintuc','middleware'=>'login'], function () {
        Route::get('/', 'TintucController@ds');
        Route::get('them', 'TintucController@get_them')->middleware('admin');
        Route::post('them', 'TintucController@post_them')->middleware('admin');
        Route::get('sua/{id}', 'TintucController@get_sua')->middleware('admin');
        Route::post('sua/{id}', 'TintucController@post_sua')->middleware('admin');
        Route::get('xem/{id}', 'TintucController@xem');
        Route::get('xoa/{id}', 'TintucController@xoa')->middleware('admin');
    });
    Route::group(['prefix' => 'thongke','middleware'=>['login','admin']], function () {
        Route::get('/', 'ThongkeController@thongke');
    });
