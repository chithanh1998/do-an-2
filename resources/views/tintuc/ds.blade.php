@extends('layout.main')
@section('content_header')
Tin tức
@endsection
@section('content')
<div class="col-xl-12 col-lg-12 order-lg-3 order-xl-1">
									<!--begin:: Widgets/Best Sellers-->
									<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Danh sách tin tức
										</h3>
									</div>
									@if(session('role') == 'admin')
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="dropdown dropdown-inline">
												</div>
												&nbsp;
												<a href="tintuc/them" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Thêm tin tức
												</a>
											</div>
										</div>
									</div>
									@endif
								</div>
										<div class="kt-portlet__body">
											<div class="tab-content">
												<div class="tab-pane active" id="kt_widget5_tab1_content" aria-expanded="true">
													<div class="kt-widget5">
													@foreach($tintuc as $tt)
														<div class="kt-widget5__item">
															<div class="kt-widget5__content">
																<div class="kt-widget5__pic">
																	<img class="kt-widget7__img" src="@if(!empty($tt->hinhnho)) images/{{$tt->hinhnho}} @endif" alt="">
																</div>
																<div class="kt-widget5__section">
																	<a href="tintuc/xem/{{$tt->idtt}}" class="kt-widget5__title">
																		{{$tt->tieude}}
																	</a>
																	<div class="kt-widget5__info">
																		<span>Ngày đăng:</span>
																		<span class="kt-font-info">{{$tt->created_at}}</span>
																	</div>
																</div>
															</div>
															<div class="kt-widget5__content">
															@if(session('role') == 'admin')
                                                            <a href="tintuc/sua/{{$tt->idtt}}" class="btn btn-sm btn-label-success btn-bold">Chỉnh sửa</a>
                                                            <a href="tintuc/xoa/{{$tt->idtt}}" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-sm btn-label-danger btn-bold kt-margin-l-5">Xóa</a>
															@endif
															</div>
														</div>
													@endforeach
													</div>
												</div>
											</div>
										</div>
									</div>

									<!--end:: Widgets/Best Sellers-->
                                </div>
@endsection
@section('script')
<script>
@if(session('noti'))
	toastr.success("{{session('noti')}}");
@endif
function confirmationDelete(anchor) {
    var conf = swal.fire({   
        title: "Bạn có chắc muốn xóa?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Có",
        cancelButtonText: "Không",   
    }).then(function(result){
		if (result.value) {   
		window.location = anchor.attr("href");
		}
    });   
}
</script>
@endsection