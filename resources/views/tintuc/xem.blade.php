@extends('layout.main')
@section('content_header')
Tin tức
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<div class="kt-portlet">
								<div class="kt-portlet__body">
									<div class="kt-infobox">
										<div class="kt-infobox__header">
											<h2 class="kt-infobox__title">{{$tintuc->tieude}}</h2>
										</div>
										<div class="kt-infobox__body">
											{!!$tintuc->noidung!!}
										</div>
									</div>
								</div>
                            </div>
						</div>
@endsection