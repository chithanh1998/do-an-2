@extends('layout.main')
@section('css')
<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection
@section('content_header')
Tìm kiếm
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
    @if(session('role') == 'admin')
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Kết quả tìm kiếm cho sinh viên
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body kt-portlet__body--fit" style="margin:10px">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered" id="kt_table_1">
										<thead>
											<tr>
                                                <th>Tên sinh viên</th>
												<th>Ngày sinh</th>
                                                <th>Điện thoại</th>
                                                <th>Địa chỉ</th>
												<th>Email</th>
												<th>Quản lý</th>
											</tr>
										</thead>
										<tbody>
                                        @if(count($sv) > 0)
										@foreach($sv as $s)
											<tr>
                                                <td>{{$s->hoten}}</td>
                                                <td>{{$s->ngaysinh}}</td>
                                                <td>{{$s->sdt}}</td>
												<td>{{$s->diachi}}</td>
												<td>{{$s->email}}</td>
												<td nowrap>@if(session('role') == 'gv' || session('role') == 'admin') <a href="lop/{{$s->idlop}}/sinhvien/xoa/{{$s->idsv}}" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Xóa"><i class="la la-trash"></i></a> @endif</td>
											</tr>
                                        @endforeach
                                        @endif
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
                            </div>
                            @endif
                            @if(count($bai) > 0)
                            <div class="row">
                            <div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Kết quả tìm kiếm bài học
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body kt-portlet__body--fit" style="margin:10px">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered" id="kt_table_2">
										<thead>
											<tr>
                                                <th>Tên bài học</th>
												<th>Ngày tạo</th>
												<th>Ngày kết thúc</th>
												<th>Quản lý</th>
											</tr>
										</thead>
										<tbody>
                                        @foreach($bai as $bh)
											<tr>
                                                <td>{{$bh->tenbaihoc}}</td>
												<td>{{$bh->ngaytao}}</td>
												<td>{{$bh->ngayketthuc}}</td>
												<td nowrap>
                                                <a href="lop/{{$bh->idlop}}/baihoc/xem/{{$bh->idbh}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Xem">
                                                        <i class="la la-eye"></i>
					                                </a>
													@if(session('role') == 'gv' || session('role') == 'admin')
                                                <a href="lop/{{$bh->idlop}}/baihoc/sua/{{$bh->idbh}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Chỉnh sửa">
                                                        <i class="la la-edit"></i>
					                                </a>
					                                <a href="lop/{{$bh->idlop}}/baihoc/xoa/{{$bh->idbh}}" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Xóa"><i class="la la-trash"></i></a>
													@endif
												</td>
											</tr>
                                        @endforeach
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>
@endif
@if(count($data) > 0)
@foreach($data as $dt)
                            <div class="row">
                            <h3 class="kt-subheader__title">Kết quả tìm kiếm lớp học</h3>
								<div class="col-lg-12">

									<!--begin::Portlet-->
									<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Lớp: {{$dt['ten']}} &nbsp&nbspGV: {{$dt['gv']}}
										</h3>
									</div>
								</div>
                            <div class="row kt-padding-10">
								<div class="col-lg-4">
									<div class="kt-portlet kt-callout kt-callout--success">
										<div class="kt-portlet__body">
											<div class="kt-callout__body">
												<div class="kt-callout__content">
													<h3 class="kt-callout__title">{{$dt['sv']}}</h3>
													<p class="kt-callout__desc">
														Sinh viên
													</p>
												</div>
												<div class="kt-callout__action">
													<a href="lop/{{$dt['id']}}/sinhvien" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Xem</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="kt-portlet kt-callout kt-callout--danger">
										<div class="kt-portlet__body">
											<div class="kt-callout__body">
												<div class="kt-callout__content">
													<h3 class="kt-callout__title">{{$dt['bai']}}</h3>
													<p class="kt-callout__desc">
														Bài học
													</p>
												</div>
												<div class="kt-callout__action">
													<a href="/lop/{{$dt['id']}}/baihoc" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-danger">Xem</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="kt-portlet kt-callout kt-callout--brand">
										<div class="kt-portlet__body">
											<div class="kt-callout__body">
												<div class="kt-callout__content">
													<h3 class="kt-callout__title">{{$dt['bl']}}</h3>
													<p class="kt-callout__desc">
														Bình luận
													</p>
												</div>
												<div class="kt-callout__action">
													<a href="lop/{{$dt['id']}}/binhluan" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-brand">Xem</a>
												</div>
											</div>
										</div>
									</div>
                                </div>
							</div>
										
</div>
@endforeach
@endif
                        </div>
						
@endsection
@section('js')
<script src="assets/plugins/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
<script>
@if(session('noti'))
toastr.success("Xóa thành công");
@endif
var KTDatatablesDataSourceHtml = function() {

var initTable1 = function() {
    var table = $('#kt_table_1');

    // begin first table
    table.DataTable({
        responsive: true,
        "language": {
            "lengthMenu": "Hiển thị _MENU_ giá trị",
            "zeroRecords": "Không tìm thấy giá trị nào",
            "info": "Hiển thị trang _PAGE_ trên _PAGES_",
            "infoEmpty": "Không có giá trị nào",
            "infoFiltered": "(Lọc từ _MAX_ giá trị)",
            "search": "Tìm kiếm:",
            "paginate": {
                "first":      "Đầu",
                "last":       "Cuối",
                "next":       "Tiếp",
                "previous":   "Trước"
            },
        },
    });
    var table2 = $('#kt_table_2');
    table2.DataTable({
        responsive: true,
        "language": {
            "lengthMenu": "Hiển thị _MENU_ giá trị",
            "zeroRecords": "Không tìm thấy giá trị nào",
            "info": "Hiển thị trang _PAGE_ trên _PAGES_",
            "infoEmpty": "Không có giá trị nào",
            "infoFiltered": "(Lọc từ _MAX_ giá trị)",
            "search": "Tìm kiếm:",
            "paginate": {
                "first":      "Đầu",
                "last":       "Cuối",
                "next":       "Tiếp",
                "previous":   "Trước"
            },
        },
    });

};

return {

    //main function to initiate the module
    init: function() {
        initTable1();
    }
};
}();
jQuery(document).ready(function() {
	KTDatatablesDataSourceHtml.init();
});
$('.kt-select2').css('width', '100%');
$('#themsv').select2({
    placeholder: "Chọn sinh viên",
});
$('.select2-selection__arrow').css('top', '14px');
$('.select2-selection__rendered').css('line-height', '10px');
function confirmationDelete(anchor) {
    var conf = swal.fire({   
        title: "Bạn có chắc muốn xóa?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Có",
        cancelButtonText: "Không",   
    }).then(function(result){
		if (result.value) {   
		window.location = anchor.attr("href");
		}
    });   
}
</script>
@endsection