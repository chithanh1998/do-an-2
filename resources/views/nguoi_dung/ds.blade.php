@extends('layout.main')
@section('css')
<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection
@section('content_header')
Người dùng
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Danh sách người dùng
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="dropdown dropdown-inline">
												</div>
												&nbsp;
												<a href="nguoidung/them" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Thêm người dùng
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body kt-portlet__body--fit" style="margin:10px">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered" id="kt_table_1">
										<thead>
											<tr>
                                                <th>Tên tài khoản</th>
												<th>Tên</th>
												<th>Ngày sinh</th>
												<th>Giới tính</th>
												<th>Địa chỉ</th>
												<th>Email</th>
												<th>Số điện thoại</th>
												<th>Quyền</th>
												<th>Quản lý</th>
											</tr>
										</thead>
										<tbody>
                                        @foreach($admin as $ad)
											<tr>
                                                <td>{{$ad->tentk}}</td>
												<td>{{$ad->hoten}}</td>
												<td>{{$ad->ngaysinh}}</td>
												@if($ad->gioitinh == 1)
                                                <td><span class="kt-badge kt-badge--inline kt-badge--danger">Nam</span></td>
                                                @else
                                                <td><span class="kt-badge kt-badge--inline kt-badge--success">Nữ</span></td>
                                                @endif
												<td>{{$ad->diachi}}</td>
												<td>{{$ad->email}}</td>
												<td>{{$ad->sdt}}</td>
												<td><span class="kt-badge kt-badge--inline kt-badge--success">Quản trị</span></td>
												<td nowrap>
                                                    <a href="nguoidung/sua/{{$ad->idtk}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Chỉnh sửa">
                                                        <i class="la la-edit"></i>
					                                </a>
					                                <a href="nguoidung/xoa/{{$ad->idtk}}" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Xóa"><i class="la la-trash"></i></a>
                                                </td>
											</tr>
                                        @endforeach
                                        @foreach($gv as $g)
											<tr>
                                                <td>{{$g->tentk}}</td>
												<td>{{$g->hoten}}</td>
												<td>{{$g->ngaysinh}}</td>
                                                @if($g->gioitinh == 1)
                                                <td><span class="kt-badge kt-badge--inline kt-badge--danger">Nam</span></td>
                                                @else
                                                <td><span class="kt-badge kt-badge--inline kt-badge--success">Nữ</span></td>
                                                @endif
												<td>{{$g->diachi}}</td>
												<td>{{$g->email}}</td>
												<td>{{$g->sdt}}</td>
												<td><span class="kt-badge kt-badge--inline kt-badge--warning">Giáo viên</span></td>
												<td nowrap>
                                                    <a href="nguoidung/sua/{{$g->idtk}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Chỉnh sửa">
                                                        <i class="la la-edit"></i>
					                                </a>
					                                <a href="nguoidung/xoa/{{$g->idtk}}" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Xóa"><i class="la la-trash"></i></a>
                                                </td>
											</tr>
                                        @endforeach
                                        @foreach($sv as $s)
											<tr>
                                                <td>{{$s->tentk}}</td>
												<td>{{$s->hoten}}</td>
												<td>{{$s->ngaysinh}}</td>
                                                @if($s->gioitinh == 1)
                                                <td><span class="kt-badge kt-badge--inline kt-badge--danger">Nam</span></td>
                                                @else
                                                <td><span class="kt-badge kt-badge--inline kt-badge--success">Nữ</span></td>
                                                @endif												<td>{{$s->diachi}}</td>
												<td>{{$s->email}}</td>
												<td>{{$s->sdt}}</td>
												<td><span class="kt-badge kt-badge--inline kt-badge--danger">Sinh viên</span></td>
												<td nowrap>
                                                    <a href="nguoidung/sua/{{$s->idtk}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Chỉnh sửa">
                                                        <i class="la la-edit"></i>
					                                </a>
					                                <a href="nguoidung/xoa/{{$s->idtk}}" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Xóa"><i class="la la-trash"></i></a>
                                                </td>
											</tr>
                                        @endforeach
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
                        </div>
@endsection
@section('js')
<script src="assets/plugins/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
<script>
@if(session('noti'))
	toastr.info("{{session('noti')}}");
@endif
var KTDatatablesDataSourceHtml = function() {

var initTable1 = function() {
    var table = $('#kt_table_1');

    // begin first table
    table.DataTable({
        responsive: true,
        "language": {
            "lengthMenu": "Hiển thị _MENU_ giá trị",
            "zeroRecords": "Không tìm thấy giá trị nào",
            "info": "Hiển thị trang _PAGE_ trên _PAGES_",
            "infoEmpty": "Không có giá trị nào",
            "infoFiltered": "(Lọc từ _MAX_ giá trị)",
            "search": "Tìm kiếm:",
            "paginate": {
                "first":      "Đầu",
                "last":       "Cuối",
                "next":       "Tiếp",
                "previous":   "Trước"
            },
        },
    });

};

return {

    //main function to initiate the module
    init: function() {
        initTable1();
    }
};
}();
jQuery(document).ready(function() {
	KTDatatablesDataSourceHtml.init();
});
function confirmationDelete(anchor) {
    var conf = swal.fire({   
        title: "Bạn có chắc muốn xóa?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Có",
        cancelButtonText: "Không",   
    }).then(function(result){
		if (result.value) {   
		window.location = anchor.attr("href");
		} 
    });   
}
</script>
@endsection