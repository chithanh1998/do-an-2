@extends('layout.main')
@section('content_header')
Người dùng
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
<!--begin::Portlet-->
<div class="row">
								<div class="col-lg-6">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Sửa người dùng
												</h3>
											</div>
										</div>

										<!--begin::Form-->
										<form class="kt-form kt-form--label-right" id="kt_form_1">
											<div class="kt-portlet__body">
												<div class="form-group form-group-last kt-hide">
													<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
														<div class="alert-icon"><i class="flaticon-warning"></i></div>
														<div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																<span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
                                                </div>
                                                <div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Họ tên</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<input type="text" class="form-control" name="name" id="name" placeholder="" value="{{$data->hoten}}">
													</div>
                                                </div>
                                                <div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Ngày sinh</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<div class="input-group date">
															<input type="text" class="form-control" id="date_birth" readonly placeholder="" name="ngaysinh" value="{{$data->ngaysinh}}"/>
																<div class="input-group-append">
																	<span class="input-group-text">
																		<i class="la la-calendar-check-o"></i>
																	</span>
																</div>
														</div>													
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Giới tính</label>
													<div class="col-lg-9 col-md-9 col-sm-12 form-group-sub">
													<div class="kt-radio-list">
														<label class="kt-radio">
															<input type="radio" name="gt" value="1" @if($data->gioitinh == 1) checked @endif> Nam
															<span></span>
														</label>
														<label class="kt-radio">
															<input type="radio" name="gt" value="2" @if($data->gioitinh == 2) checked @endif> Nữ
															<span></span>
														</label>
													</div>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Email</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<input type="text" class="form-control" name="email" id="email" value="{{$data->email}}" placeholder="">
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Điện thoại</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<div class="input-group">
															<input type="number" class="form-control" name="phone" id="phone" placeholder="" value="{{$data->sdt}}">
															<div class="input-group-append"><span class="btn btn-brand btn-icon"><i class="la la-phone"></i></span></div>
														</div>
													</div>
                                                </div>
                                                <div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Địa chỉ</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<input type="text" class="form-control" name="address" id="address" placeholder="" value="{{$data->diachi}}">
													</div>
                                                </div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3 col-sm-12">Quyền</label>
													<div class="col-lg-9 col-md-9 col-sm-12 form-group-sub">
														<select class="form-control" name="option" id="role" disabled>
															<option value="1" @if($role == 1) selected @endif>Quản trị</option>
															<option value="2" @if($role == 2) selected @endif>Giáo viên</option>
															<option value="3" @if($role == 3) selected @endif>Sinh viên</option>
														</select>
													</div>
												</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<div class="row">
														<div class="col-lg-9 ml-lg-auto">
															<button type="button" class="btn btn-brand" id="save">Lưu</button>
														</div>
													</div>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

                                    <!--end::Portlet-->
                                    </div>
							</div>
						</div>
@endsection
@section('script')
<script>
    function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "1500",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut",
  "preventDuplicates": true,
};
$('#date_birth').datepicker({
            orientation: "bottom left",
			toggleActive: false,
        	format: 'yyyy-mm-dd',
        	autoclose: true,
        	startView: 2,
        });
$('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var name = $('#name').val();
        var birth = $('#date_birth').val();
        var email = $('#email').val();
        var role = $('#role').val();
        var phone = $('#phone').val();
		var address = $('#address').val();
		var gender = $('input[name=gt]:checked', '#kt_form_1').val();
        $.ajax({
            type: 'post',
            url: 'nguoidung/sua/'+{{$data->idtk}},
            data: {
                name: name, email: email, role: role, phone: phone, address: address, date_birth: birth, gender: gender
            },
            beforeSend: function(){              
                if(name == "" || email == "" || phone == "" || address == "" || birth == "" || gender == ""){
                    toastr.info("Hãy nhập tất cả các trường");
                    return false;
                }
                if(validateEmail(email) == 0){
                    toastr.info("Email không đúng");
                    return false;
                }
                $('#save').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
            },
            success: function(resp){
				$('#save').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
				if(resp == "ok"){
                toastr.success("Lưu thành công");
				setTimeout('window.location.href = "nguoidung";',1500);
				} else {
				}
            }
        })
    })
</script>
@endsection