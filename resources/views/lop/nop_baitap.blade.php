@extends('layout.main')
@section('content_header')
Lớp {{$lop->tenlop}}
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
<!--begin::Portlet-->
<div class="row">
								<div class="col-lg-12">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Nộp bài tập
												</h3>
											</div>
										</div>

										<!--begin::Form-->
										<form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" enctype="multipart/form-data" action="lop/{{$lop->idlop}}/baihoc/{{$id_bh}}/nopbaitap">
										@csrf
											<div class="kt-portlet__body">
												<div class="form-group form-group-last kt-hide">
													<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
														<div class="alert-icon"><i class="flaticon-warning"></i></div>
														<div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																<span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
                                                </div>
                                                <div class="kt-section">
												<span class="kt-section__info">
													Bài tập đã nộp
												</span>
												<div class="kt-section__content">
													<table class="table">
														<thead>
															<tr>
																<th>Tiêu đề</th>
                                                                <th>Tên file</th>
                                                                <th>Thời gian</th>
																<th>Quản lý</th>
															</tr>
														</thead>
														<tbody>
														@if($dsbt->isEmpty())
														<tr>
															<td>Chưa nộp bài tập nào</td>
															<td></td>
														</tr>
														@endif
														@foreach($dsbt as $bt)
															<tr>
																<td>{{$bt->tieude}}</td>
                                                                <td>{{$bt->file}}</td>
                                                                <td>{{$bt->created_at}}</td>
																<td nowrap><a href="lop/xoabaitap/{{$bt->idbt}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="javascript:confirmationDelete($(this));return false;" title="Xóa"><i class="la la-trash"></i></a></td>
															</tr>
														@endforeach
														</tbody>
													</table>
												</div>
                                                <div class="form-group row">
													<label class="col-form-label col-lg-1 col-sm-12">Tiêu đề</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<input type="text" class="form-control" name="tieude" id="name" placeholder="" required>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-1 col-sm-12">Bài tập</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
													<div class="custom-file">
														<input type="file" class="custom-file-input" id="customFile" name="tailieu">
														<label class="custom-file-label" for="customFile" style="text-align: left"></label>
													</div>
													</div>
                                                </div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<div class="row">
														<div class="col-lg-9 ml-lg-auto">
															<button type="submit" class="btn btn-brand" id="save">Lưu</button>
														</div>
													</div>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

                                    <!--end::Portlet-->
                                    </div>
							</div>
						</div>
@endsection
@section('js')
@endsection
@section('script')
<script>
@if(session('noti'))
toastr.success("{{session('noti')}}");
@endif
@if(session('err'))
toastr.info("{{session('err')}}");
@endif
function confirmationDelete(anchor) {
    var conf = swal.fire({   
        title: "Bạn có chắc muốn xóa?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Có",
        cancelButtonText: "Không",   
    }).then(function(result){
		if (result.value) {   
		window.location = anchor.attr("href");
		} 
    });   
}
</script>
@endsection
