@extends('layout.main')
@section('css')
<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection
@section('content_header')
Lớp {{$lop->tenlop}}
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Danh sách nộp bài tập
										</h3>
									</div>
								</div>
								<div class="kt-portlet__body kt-portlet__body--fit" style="margin:10px">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered" id="kt_table_1">
										<thead>
											<tr>
                                                <th>Tên sinh viên</th>
												<th>Tiêu đề</th>
                                                <th>Thời gian nộp</th>
												<th>Quản lý</th>
											</tr>
										</thead>
										<tbody>
										@foreach($ds as $s)
											<tr>
                                                <td>{{$s->hoten}}</td>
                                                <td>{{$s->tieude}}</td>
                                                <td>{{$s->created_at}}</td>
												<td nowrap><a href="upload/{{$s->file}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Tải xuống"><i class="la la-arrow-down"></i></a></td>
											</tr>
										@endforeach
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
                        </div>
@endsection
@section('js')
<script src="assets/plugins/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
<script>
@if(session('noti'))
toastr.success("Xóa thành công");
@endif
var KTDatatablesDataSourceHtml = function() {

var initTable1 = function() {
    var table = $('#kt_table_1');

    // begin first table
    table.DataTable({
        responsive: true,
        "language": {
            "lengthMenu": "Hiển thị _MENU_ giá trị",
            "zeroRecords": "Không tìm thấy giá trị nào",
            "info": "Hiển thị trang _PAGE_ trên _PAGES_",
            "infoEmpty": "Không có giá trị nào",
            "infoFiltered": "(Lọc từ _MAX_ giá trị)",
            "search": "Tìm kiếm:",
            "paginate": {
                "first":      "Đầu",
                "last":       "Cuối",
                "next":       "Tiếp",
                "previous":   "Trước"
            },
        },
    });

};

return {

    //main function to initiate the module
    init: function() {
        initTable1();
    }
};
}();
jQuery(document).ready(function() {
	KTDatatablesDataSourceHtml.init();
});
</script>
@endsection