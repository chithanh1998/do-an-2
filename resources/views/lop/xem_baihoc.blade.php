@extends('layout.main')
@section('content_header')
Lớp {{$lop->tenlop}}
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<div class="kt-portlet">
								<div class="kt-portlet__body">
									<div class="kt-infobox">
										<div class="kt-infobox__header">
											<h2 class="kt-infobox__title">{{$bh->tenbaihoc}}</h2>
										</div>
										<div class="kt-infobox__body">
											{!!$bh->noidung!!}
										</div>
									</div>
								</div>
                            </div>
                            <div class="row">
<div class="col-xl-12">

									<!--begin:: Widgets/Download Files-->
									<div class="kt-portlet kt-portlet--height-fluid">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Tệp đính kèm
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">

											<!--begin::k-widget4-->
											<div class="kt-widget4">
                                                @foreach($tailieu as $tl)
												<div class="kt-widget4__item">
													<div class="kt-widget4__pic kt-widget4__pic--icon">
                                                        <i class="la la-file"></i>
													</div>
													<a href="tailieu/{{$tl->duongdan}}" class="kt-widget4__title">
														{{$tl->duongdan}}
													</a>
                                                </div>
                                                @endforeach
											</div>

											<!--end::Widget 9-->
										</div>
									</div>

									<!--end:: Widgets/Download Files-->
								</div>
                            </div>
						</div>
@endsection