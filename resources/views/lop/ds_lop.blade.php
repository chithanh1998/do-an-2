@extends('layout.main')
@section('css')
<link rel='stylesheet prefetch' href='https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css'>
<style>
div.stars {
  width: 270px;
  display: inline-block;
}
 
input.star { display: none; }
 
label.star {
  float: right;
  padding: 10px;
  font-size: 36px;
  color: #444;
  transition: all .2s;
}
 
input.star:checked ~ label.star:before {
  content: '\f005';
  color: #FD4;
  transition: all .25s;
}
 
input.star-5:checked ~ label.star:before {
  color: #FE7;
  text-shadow: 0 0 20px #952;
}
 
input.star-1:checked ~ label.star:before { color: #F62; }
 
label.star:hover { transform: rotate(-15deg) scale(1.3); }
 
label.star:before {
  content: '\f006';
  font-family: FontAwesome;
}
</style>
@endsection
@section('content_header')
Lớp
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
@if(session('role') == 'admin')
	<div class="row">
	<a href="#" class="btn btn-brand btn-elevate btn-icon-sm kt-margin-b-20" data-toggle="modal" data-target="#kt_modal_4">
		<i class="la la-plus"></i>
		Thêm lớp
	</a>
</div>
@endif
<!--begin::Portlet-->
@foreach($data as $dt)
<div class="row">
								<div class="col-lg-12">

									<!--begin::Portlet-->
									<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Lớp: {{$dt['ten']}} &nbsp&nbspGV: {{$dt['gv']}}
										</h3>
									</div>
									@if(session('role') == 'admin')
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="dropdown dropdown-inline">
												</div>
												&nbsp;
												<a id="{{$dt['id']}}" style="color:white" class="btn btn-brand btn-elevate btn-icon-sm sualop" data-toggle="modal" data-target="#kt_modal_5">
													<i class="la la-edit"></i>
													Sửa
												</a>
												<a href="lop/{{$dt['id']}}/xoalop" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-trash"></i>
													Xóa
												</a>
											</div>
										</div>
									</div>
									@endif
								</div>
                            <div class="row kt-padding-10">
								<div class="col-lg-3">
									<div class="kt-portlet kt-callout kt-callout--success">
										<div class="kt-portlet__body">
											<div class="kt-callout__body">
												<div class="kt-callout__content">
													<h3 class="kt-callout__title">{{$dt['sv']}}</h3>
													<p class="kt-callout__desc">
														Sinh viên
													</p>
												</div>
												<div class="kt-callout__action">
													<a href="lop/{{$dt['id']}}/sinhvien" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success">Xem</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<div class="kt-portlet kt-callout kt-callout--danger">
										<div class="kt-portlet__body">
											<div class="kt-callout__body">
												<div class="kt-callout__content">
													<h3 class="kt-callout__title">{{$dt['bai']}}</h3>
													<p class="kt-callout__desc">
														Bài học
													</p>
												</div>
												<div class="kt-callout__action">
													<a href="/lop/{{$dt['id']}}/baihoc" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-danger">Xem</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-3">
									<div class="kt-portlet kt-callout kt-callout--brand">
										<div class="kt-portlet__body">
											<div class="kt-callout__body">
												<div class="kt-callout__content">
													<h3 class="kt-callout__title">{{$dt['bl']}}</h3>
													<p class="kt-callout__desc">
														Bình luận
													</p>
												</div>
												<div class="kt-callout__action">
													<a href="lop/{{$dt['id']}}/binhluan" class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-brand">Xem</a>
												</div>
											</div>
										</div>
									</div>
                                </div>
                                <div class="col-lg-3">
									<div class="kt-portlet kt-callout kt-callout--success">
										<div class="kt-portlet__body">
											<div class="kt-callout__body">
												<div class="kt-callout__content">
													<h3 class="kt-callout__title">@php echo round($dt['danhgia'],1); @endphp <i class="fa fa-star"></i></h3>
													<p class="kt-callout__desc">
														Đánh giá
													</p>
												</div>
												<div class="kt-callout__action">
													<a class="btn btn-custom btn-bold btn-upper btn-font-sm  btn-success nutdg" style="color:white" id="{{$dt['id']}}" data-toggle="modal" data-target="#kt_modal_1">Đánh giá</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
										
									</div>
									@endforeach
                                    <!--end::Portlet-->
                                    </div>
							</div>
						</div>
						@if(session('role') == 'admin')
						<!--begin::Modal-->
						<div class="modal fade" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Thêm lớp</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<form>
												<div class="form-group">
													<label for="recipient-name" class="form-control-label">Tên lớp:</label>
													<input type="text" class="form-control" id="ten">
												</div>
												<div class="form-group">
                                                    <label for="recipient-name" class="form-control-label">Giáo viên:</label>
                                                    <div clas="row">
													<select class="form-control kt-select2" id="gv" name="param">
													@foreach($dsgv as $gv)
                                                    <option value="{{$gv->idgv}}">{{$gv->hoten}}</option>
													@endforeach
                                                    </select>
                                                    </div>
												</div>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
											<button type="button" class="btn btn-primary" id="add">Thêm</button>
										</div>
									</div>
								</div>
							</div>

							<!--end::Modal-->

							<!--begin::Modal-->
						<div class="modal fade" id="kt_modal_5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Sửa lớp</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<form>
												<div class="form-group">
													<label for="recipient-name" class="form-control-label">Tên lớp:</label>
													<input type="text" class="form-control" id="suaten">
												</div>
												<div class="form-group">
                                                    <label for="recipient-name" class="form-control-label">Giáo viên:</label>
                                                    <div clas="row">
													<select class="form-control kt-select2" id="suagv" name="param">
                                                    @foreach($dsgv as $gv)
                                                    <option value="{{$gv->idgv}}">{{$gv->hoten}}</option>
													@endforeach
                                                    </select>
                                                    </div>
												</div>
												<input type="hidden" id="lop_id">
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
											<button type="button" class="btn btn-primary" id="save">Lưu</button>
										</div>
									</div>
								</div>
							</div>
@endif
							<!--end::Modal-->
							<!--begin::Modal-->
							<div class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Đánh giá</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
										<div class="stars">
										  <form id="dg">
										    <input class="star star-5" id="star-5" type="radio" name="star" value="5"/>
										    <label class="star star-5" for="star-5"></label>
										    <input class="star star-4" id="star-4" type="radio" name="star" value="4"/>
										    <label class="star star-4" for="star-4"></label>
										    <input class="star star-3" id="star-3" type="radio" name="star" value="3"/>
										    <label class="star star-3" for="star-3"></label>
										    <input class="star star-2" id="star-2" type="radio" name="star" value="2"/>
										    <label class="star star-2" for="star-2"></label>
										    <input class="star star-1" id="star-1" type="radio" name="star" value="1"/>
											<label class="star star-1" for="star-1"></label>
											<input type="hidden" id="idlop"/>
										  </form>
										</div>
										<input type="hidden" id="sao"/>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
											<button type="button" class="btn btn-primary" id="danhgia">Đánh giá</button>
										</div>
									</div>
								</div>
							</div>

							<!--end::Modal-->
@endsection
@section('js')
@section('script')
<script>
@if(session('noti'))
	toastr.info("{{session('noti')}}");
@endif
$(".nutdg").click(function(){
// Lấy thông tin bus
        var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: 'lop/layid',
            data:{
                id: id
            },
            success: function(resp){
                $('#idlop').val(resp);
            }
        })
    })
$('#danhgia').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var idlop = $('#idlop').val();
		var danhgia = $('input[name=star]:checked', '#dg').val();
		console.log(danhgia);
        $.ajax({
            type: 'post',
            url: 'lop/danhgia',
            data: {
                idlop: idlop, danhgia: danhgia,
            },
            beforeSend: function(){              
                $('#danhgia').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
            },
            success: function(resp){
				$('#danhgia').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
				if(resp == "ok"){
                toastr.success("Đánh giá thành công");
				} else {
					toastr.info("Bạn đã đánh giá rồi");
				}
            }
        })
    })
$('.sualop').click(function(){
	var id = $(this).attr("id");
        $.ajax({
            type: 'get',
            url: 'lop/'+id+'/sualop',
            dataType: 'json',
            data:{
                id: id
            },
            success: function(resp){
                $('#suaten').val(resp.tenlop);
                $('#suagv').val(resp.idgv).trigger('change');
                $('#lop_id').val(resp.idlop);
            }
        })
})
$('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var tenlop = $('#ten').val();
        var idgv = $('#gv').val();
        $.ajax({
            type: 'post',
            url: 'lop/themlop',
            data: {
                tenlop: tenlop, idgv: idgv,
            },
            beforeSend: function(){              
                if(tenlop == ""){
                    toastr.info("Hãy nhập tất cả các trường");
                    return false;
                }
                $('#add').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
            },
            success: function(resp){
				$('#add').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
				if(resp == "ok"){
                toastr.success("Thêm thành công");
				setTimeout('window.location.href = "lop";',1500);
				} else {
					toastr.info("Tên lớp đã tồn tại");
				}
            }
        })
    })
$('#save').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var tenlop = $('#suaten').val();
        var idgv = $('#suagv').val();
        var idlop = $('#lop_id').val();
        $.ajax({
            type: 'post',
            url: 'lop/'+idlop+'/sualop',
            data: {
                tenlop: tenlop, idgv: idgv, idlop: idlop
            },
            beforeSend: function(){              
                if(tenlop == ""){
                    toastr.info("Hãy nhập tất cả các trường");
                    return false;
                }
                $('#save').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
            },
            success: function(resp){
				$('#save').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
				if(resp == "ok"){
                toastr.success("Sửa thành công");
				setTimeout('window.location.href = "lop";',1500);
				} else {
					toastr.info("Tên lớp đã tồn tại");
				}
            }
        })
    })
$('.kt-select2').css('width', '100%');
$('#gv').select2({
    placeholder: "Chọn giáo viên",
});
$('#suagv').select2({
    placeholder: "Chọn giáo viên",
});
$('.select2-selection__arrow').css('top', '14px');
$('.select2-selection__rendered').css('line-height', '10px');
function confirmationDelete(anchor) {
    var conf = swal.fire({   
        title: "Bạn có chắc muốn xóa?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Có",
        cancelButtonText: "Không",   
    }).then(function(result){
		if (result.value) {   
		window.location = anchor.attr("href");
		}
    });   
}

</script>
@endsection