@extends('layout.main')
@section('css')
<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection
@section('content_header')
Lớp {{$lop->tenlop}}
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Danh sách sinh viên
										</h3>
									</div>
									@if(session('role') == 'gv' || session('role') == 'admin')
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="dropdown dropdown-inline">
												</div>
												&nbsp;
												<a href="#" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#kt_modal_4">
													<i class="la la-plus"></i>
													Thêm sinh viên
												</a>
											</div>
										</div>
									</div>
									@endif
								</div>
								<div class="kt-portlet__body kt-portlet__body--fit" style="margin:10px">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered" id="kt_table_1">
										<thead>
											<tr>
                                                <th>Tên sinh viên</th>
												<th>Ngày sinh</th>
                                                <th>Điện thoại</th>
                                                <th>Địa chỉ</th>
												<th>Email</th>
												<th>Quản lý</th>
											</tr>
										</thead>
										<tbody>
										@foreach($sv as $s)
											<tr>
                                                <td>{{$s->hoten}}</td>
                                                <td>{{$s->ngaysinh}}</td>
                                                <td>{{$s->sdt}}</td>
												<td>{{$s->diachi}}</td>
												<td>{{$s->email}}</td>
												<td nowrap>@if(session('role') == 'gv' || session('role') == 'admin') <a href="lop/{{$s->idlop}}/sinhvien/xoa/{{$s->idsv}}" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Xóa"><i class="la la-trash"></i></a> @endif</td>
											</tr>
										@endforeach
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
                        </div>
						@if(session('role') == 'gv' || session('role') == 'admin')
                        <!--begin::Modal-->
							<div class="modal fade" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Thêm sinh viên</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<form>
												<div class="form-group">
                                                    <label for="recipient-name" class="form-control-label">Sinh viên:</label>
                                                    <div clas="row">
													<select class="form-control kt-select2" id="themsv" name="param">
													@foreach($dssv as $ds)
                                                    <option value="{{$ds->idsv}}">{{$ds->hoten}}</option>
													@endforeach
                                                    </select>
                                                    </div>
												</div>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
											<button type="button" class="btn btn-primary" id="add">Thêm</button>
										</div>
									</div>
								</div>
							</div>

							<!--end::Modal-->
							@endif
@endsection
@section('js')
<script src="assets/plugins/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
<script>
@if(session('noti'))
toastr.success("Xóa thành công");
@endif
var KTDatatablesDataSourceHtml = function() {

var initTable1 = function() {
    var table = $('#kt_table_1');

    // begin first table
    table.DataTable({
        responsive: true,
        "language": {
            "lengthMenu": "Hiển thị _MENU_ giá trị",
            "zeroRecords": "Không tìm thấy giá trị nào",
            "info": "Hiển thị trang _PAGE_ trên _PAGES_",
            "infoEmpty": "Không có giá trị nào",
            "infoFiltered": "(Lọc từ _MAX_ giá trị)",
            "search": "Tìm kiếm:",
            "paginate": {
                "first":      "Đầu",
                "last":       "Cuối",
                "next":       "Tiếp",
                "previous":   "Trước"
            },
        },
    });

};

return {

    //main function to initiate the module
    init: function() {
        initTable1();
    }
};
}();
jQuery(document).ready(function() {
	KTDatatablesDataSourceHtml.init();
});
$('.kt-select2').css('width', '100%');
$('#themsv').select2({
    placeholder: "Chọn sinh viên",
});
$('.select2-selection__arrow').css('top', '14px');
$('.select2-selection__rendered').css('line-height', '10px');
$('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var sv = $('#themsv').val();
        $.ajax({
            type: 'post',
            url: 'lop/'+'{{$lop->idlop}}'+'/sinhvien/them',
            data: {
                sv: sv
            },
            beforeSend: function(){              
                if(sv == ""){
                    toastr.info("Hãy nhập tất cả các trường");
                    return false;
                }
                $('#add').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
            },
            success: function(resp){
				$('#add').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
				if(resp == "ok"){
                toastr.success("Thêm thành công");
				setTimeout('window.location.href = "lop/'+'{{$lop->idlop}}'+'/sinhvien";',1500);
				} else {
				}
            }
        })
    })
function confirmationDelete(anchor) {
    var conf = swal.fire({   
        title: "Bạn có chắc muốn xóa?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Có",
        cancelButtonText: "Không",   
    }).then(function(result){
		if (result.value) {   
		window.location = anchor.attr("href");
		}
    });   
}
</script>
@endsection