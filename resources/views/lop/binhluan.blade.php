@extends('layout.main')
@section('content_header')
Lớp
@endsection
@section('content')
<div class="col-xl-12 col-lg-12 order-lg-3 order-xl-1">
									<!--begin:: Widgets/Best Sellers-->
									<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Danh sách bình luận
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="dropdown dropdown-inline">
												</div>
												&nbsp;
												<a href="#" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#kt_modal_4">
													<i class="la la-plus"></i>
													Thêm bình luận
												</a>
											</div>
										</div>
									</div>
								</div>
										<div class="kt-portlet__body">
											<div class="tab-content">
												<div class="tab-pane active" id="kt_widget5_tab1_content" aria-expanded="true">
													<div class="kt-widget5">
														@foreach($binhluan as $bl)
														<div class="kt-widget5__item">
															<div class="kt-widget5__content">
																<div class="kt-widget5__pic">

																</div>
																<div class="kt-widget5__section">
																	<a href="#" class="kt-widget5__title">
																		{{$bl->tentk}}
																	</a>
																	<p class="kt-widget5__desc">
																		{{$bl->noidung}}
																	</p>
																	<div class="kt-widget5__info">
																		<span>Thời gian:</span>
																		<span class="kt-font-info">{{$bl->created_at}}</span>
																	</div>
																</div>
															</div>
															<div class="kt-widget5__content">
															@if(session('role') == 'admin')
                                                            <a href="lop/binhluan/xoa/{{$bl->idbl}}" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-sm btn-label-danger btn-bold kt-margin-l-5">Xóa</a>
															@elseif(session('id_user') == $bl->idtk)
                                                            <a href="lop/binhluan/xoa/{{$bl->idbl}}" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-sm btn-label-danger btn-bold kt-margin-l-5">Xóa</a>
															@endif
															</div>
														</div>
														@endforeach
													</div>
												</div>
											</div>
										</div>
									</div>

									<!--end:: Widgets/Best Sellers-->
								</div>
								

								<div class="modal fade" id="kt_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Thêm bình luận</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<form>
												<div class="form-group">
													<label for="message-text" class="form-control-label">Nội dung:</label>
													<textarea class="form-control" id="noidung"></textarea>
												</div>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-primary" id="add">Lưu</button>
										</div>
									</div>
								</div>
							</div>
@endsection
@section('script')
<script>
@if(session('noti'))
	toastr.success("{{session('noti')}}");
@endif
$('#add').click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var noidung = $('#noidung').val();
        $.ajax({
            type: 'post',
            url: 'lop/{{$lop->idlop}}/binhluan/them',
            data: {
                noidung: noidung,
            },
            beforeSend: function(){              
                if(noidung == ""){
                    toastr.info("Hãy nhập bình luận");
                    return false;
                }
                $('#add').addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
            },
            success: function(resp){
				$('#add').removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light');
				if(resp == "ok"){
                toastr.success("Thêm thành công");
				setTimeout('window.location.href = "lop/{{$lop->idlop}}/binhluan";',1500);
				} else {
				}
            }
        })
	});
	function confirmationDelete(anchor) {
    var conf = swal.fire({   
        title: "Bạn có chắc muốn xóa?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Có",
        cancelButtonText: "Không",   
    }).then(function(result){
		if (result.value) {   
		window.location = anchor.attr("href");
		}
    });   
}
</script>
@endsection