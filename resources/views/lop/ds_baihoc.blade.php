@extends('layout.main')
@section('css')
<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
@endsection
@section('content_header')
Lớp {{$lop->tenlop}}
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<h3 class="kt-portlet__head-title">
											Danh sách Bài học
										</h3>
									</div>
									@if(session('role') == 'gv' || session('role') == 'admin')
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="dropdown dropdown-inline">
												</div>
												&nbsp;
												<a href="lop/{{$lop->idlop}}/baihoc/them" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Thêm bài học
												</a>
											</div>
										</div>
									</div>
									@endif
								</div>
								<div class="kt-portlet__body kt-portlet__body--fit" style="margin:10px">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered" id="kt_table_1">
										<thead>
											<tr>
                                                <th>Tên bài học</th>
												<th>Ngày tạo</th>
												<th>Ngày kết thúc</th>
												<th>Quản lý</th>
											</tr>
										</thead>
										<tbody>
                                        @foreach($bai as $bh)
											<tr>
                                                <td>{{$bh->tenbaihoc}}</td>
												<td>{{$bh->ngaytao}}</td>
												<td>{{$bh->ngayketthuc}}</td>
												<td nowrap>
                                                <a href="lop/{{$lop->idlop}}/baihoc/xem/{{$bh->idbh}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Xem">
                                                        <i class="la la-eye"></i>
					                                </a>
													@if(session('role') == 'gv' || session('role') == 'admin')
                                                <a href="lop/{{$lop->idlop}}/baihoc/sua/{{$bh->idbh}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Chỉnh sửa">
                                                        <i class="la la-edit"></i>
					                                </a>
					                                <a href="lop/{{$lop->idlop}}/baihoc/xoa/{{$bh->idbh}}" onclick="javascript:confirmationDelete($(this));return false;" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Xóa"><i class="la la-trash"></i></a>
													<a href="lop/{{$lop->idlop}}/baihoc/{{$bh->idbh}}/dsnopbaitap" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Danh sách nộp bài tập">
														<i class="la la-align-justify"></i>
					                                </a>
													@else
													<a href="lop/{{$lop->idlop}}/baihoc/{{$bh->idbh}}/nopbaitap" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Nộp bài tập">
														<i class="la la-align-justify"></i>
					                                </a>
													@endif
												</td>
											</tr>
                                        @endforeach
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
                        </div>
@endsection
@section('js')
<script src="assets/plugins/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
<script>
@if(session('noti'))
toastr.success("Xóa thành công");
@endif
var KTDatatablesDataSourceHtml = function() {

var initTable1 = function() {
    var table = $('#kt_table_1');

    // begin first table
    table.DataTable({
        responsive: true,
        "language": {
            "lengthMenu": "Hiển thị _MENU_ giá trị",
            "zeroRecords": "Không tìm thấy giá trị nào",
            "info": "Hiển thị trang _PAGE_ trên _PAGES_",
            "infoEmpty": "Không có giá trị nào",
            "infoFiltered": "(Lọc từ _MAX_ giá trị)",
            "search": "Tìm kiếm:",
            "paginate": {
                "first":      "Đầu",
                "last":       "Cuối",
                "next":       "Tiếp",
                "previous":   "Trước"
            },
        },
    });

};

return {

    //main function to initiate the module
    init: function() {
        initTable1();
    }
};
}();
jQuery(document).ready(function() {
	KTDatatablesDataSourceHtml.init();
});
function confirmationDelete(anchor) {
    var conf = swal.fire({   
        title: "Bạn có chắc muốn xóa?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Có",
        cancelButtonText: "Không",   
    }).then(function(result){
		if (result.value) {   
		window.location = anchor.attr("href");
		}
    });   
}
</script>
@endsection