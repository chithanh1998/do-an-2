@extends('layout.main')
@section('content_header')
Lớp {{$lop->tenlop}}
@endsection
@section('content')
<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
<!--begin::Portlet-->
<div class="row">
								<div class="col-lg-12">

									<!--begin::Portlet-->
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Sửa bài học
												</h3>
											</div>
										</div>

										<!--begin::Form-->
										<form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" enctype="multipart/form-data" action="lop/{{$lop->idlop}}/baihoc/sua/{{$bh->idbh}}">
										@csrf
											<div class="kt-portlet__body">
												<div class="form-group form-group-last kt-hide">
													<div class="alert alert-danger" role="alert" id="kt_form_1_msg">
														<div class="alert-icon"><i class="flaticon-warning"></i></div>
														<div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																<span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
                                                </div>
                                                <div class="form-group row">
													<label class="col-form-label col-lg-1 col-sm-12">Tên bài học</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<input type="text" class="form-control" name="tenbaihoc" id="tenbaihoc" placeholder="" value="{{$bh->tenbaihoc}}">
													</div>
                                                </div>
                                                <div class="form-group row">
													<label class="col-form-label col-lg-1 col-sm-12">Ngày tạo</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
													<div class="input-group date">
															<input type="text" class="form-control" id="start_date" name="ngaytao" readonly placeholder="" value="{{$bh->ngaytao}}"/>
																<div class="input-group-append">
																	<span class="input-group-text">
																		<i class="la la-calendar-check-o"></i>
																	</span>
																</div>
														</div>													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-1 col-sm-12">Ngày kết thúc</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
													<div class="input-group date">
															<input type="text" class="form-control" id="end_date" name="ngayketthuc" readonly placeholder="" value="{{$bh->ngayketthuc}}"/>
																<div class="input-group-append">
																	<span class="input-group-text">
																		<i class="la la-calendar-check-o"></i>
																	</span>
																</div>
														</div>													</div>
                                                </div>
                                                <div class="form-group row">
													<label class="col-form-label col-lg-1 col-sm-12">Nội dung</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
                                                        <div id="content" style="height: 325px">
														<textarea name="noidung" id="kt-ckeditor-1">{{$bh->noidung}}</textarea>
                                                        </div>
													</div>
                                                </div>
                                                <div class="kt-section">
												<span class="kt-section__info">
													Tài liệu
												</span>
												<div class="kt-section__content">
													<table class="table">
														<thead>
															<tr>
																<th>Tên</th>
																<th>Quản lý</th>
															</tr>
														</thead>
														<tbody>
														@if($tailieu->isEmpty())
														<tr>
															<td>Không có tài liệu nào</td>
															<td></td>
														</tr>
														@endif
														@foreach($tailieu as $tl)
															<tr>
																<td>{{$tl->duongdan}}</td>
																<td nowrap><a href="lop/xoatailieu/{{$tl->idtl}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" onclick="javascript:confirmationDelete($(this));return false;" title="Xóa"><i class="la la-trash"></i></a></td>
															</tr>
														@endforeach
														</tbody>
													</table>
												</div>
											</div>
                                                <div class="form-group row" id="upload">
													<label>Thêm tài liệu</label>
													<div></div>
													<div class="custom-file">
														<input type="file" name="tailieu" class="custom-file-input" id="customFile">
														<label class="custom-file-label" for="customFile" style="text-align: left">Chọn file</label>
													</div>
												</div>
											<div class="kt-portlet__foot">
												<div class="kt-form__actions">
													<div class="row">
														<div class="col-lg-9 ml-lg-auto">
															<button type="submit" class="btn btn-brand" id="save">Lưu</button>
														</div>
													</div>
												</div>
											</div>
										</form>

										<!--end::Form-->
									</div>

                                    <!--end::Portlet-->
                                    </div>
							</div>
						</div>
@endsection
@section('js')
<script src={{ url('ckeditor/ckeditor.js') }}></script>
@include('ckfinder::setup')
<script src="assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
<script>
    @if(session('noti'))
toastr.success("{{session('noti')}}");
@endif
    function confirmationDelete(anchor) {
    var conf = swal.fire({   
        title: "Bạn có chắc muốn xóa?",      
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Có",
        cancelButtonText: "Không",   
    }).then(function(result){
		if (result.value) {   
		window.location = anchor.attr("href");
		} 
    });   
}
$('#start_date').datepicker({
            orientation: "bottom left",
			toggleActive: false,
        	format: 'yyyy-mm-dd',
        	autoclose: true,
		});
	$('#end_date').datepicker({
        orientation: "bottom left",
		toggleActive: false,
    	format: 'yyyy-mm-dd',
    	autoclose: true,
	});
	CKEDITOR.replace( 'kt-ckeditor-1', {
        filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',

    } );
</script>
@endsection
