<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Taikhoan;
use App\Admin;
use App\Giaovien;
use App\Sinhvien;
use App\Lopsinhvien;
use App\Lop;

class NguoidungController extends Controller
{
    public function get_dangnhap(){
        $session = session()->get('id_user');
        if(!empty($session)){
            if(session()->get('role') == 'admin'){
                return redirect('thongke');
            } else{
                return redirect('lop');
            }
        } else {
            return view('dangnhap');
        }
    }
    public function post_dangnhap(Request $request){
        $users = Taikhoan::where('tentk', $request->username)->where('matkhau', md5($request->password))->get();
        if($users->count() == 1){
            $user = $users->first();
            $admin = Admin::where('idtk',$user->idtk)->get();
            $gv = Giaovien::where('idtk',$user->idtk)->get();
            $sv = Sinhvien::where('idtk',$user->idtk)->get();
            if($admin->count() > 0){
                session()->put('role','admin');
                session()->put('name',$admin->first()->hoten);
                echo "admin";
            } elseif ($gv->count() > 0) {
                session()->put('role','gv');
                session()->put('name',$gv->first()->hoten);
                echo "ok";
            } else {
                session()->put('role','sv');
                session()->put('name',$sv->first()->hoten);
                session()->put('idsv',$sv->first()->idsv);
                echo "ok";
            }
            session()->put('id_user', $user->idtk);    
        }
        else {
            echo "err";
        }
    }
    public function dangxuat(){
        session()->flush();
        return redirect('/');
    }
    public function ds(){
        $admin = Admin::leftjoin('taikhoan','admin.idtk','=','taikhoan.idtk')->get();
        $gv = Giaovien::leftjoin('taikhoan','giaovien.idtk','=','taikhoan.idtk')->get();
        $sv = Sinhvien::leftjoin('taikhoan','sinhvien.idtk','=','taikhoan.idtk')->get();
        return view('nguoi_dung.ds',['admin'=>$admin,'gv'=>$gv,'sv'=>$sv]);
    }
    public function get_them(){
        return view('nguoi_dung.them');
    }
    public function post_them(Request $request){
        $check = Taikhoan::where('tentk',$request->username)->count();
        if($check != 0){
            echo 'err';
        } else {
            if($request->role == 1){
                $tk = new Taikhoan;
                $tk->tentk = $request->username;
                $tk->matkhau = md5($request->password);
                $tk->save();
                $ad = new Admin;
                $ad->hoten = $request->name;
                $ad->ngaysinh = $request->date_birth;
                $ad->diachi = $request->address;
                $ad->gioitinh = $request->gender;
                $ad->email = $request->email;
                $ad->sdt = $request->phone;
                $ad->idtk = $tk->idtk;
                $ad->save();
            }
            if($request->role == 2){
                $tk = new Taikhoan;
                $tk->tentk = $request->username;
                $tk->matkhau = md5($request->password);
                $tk->save();
                $gv = new Giaovien;
                $gv->hoten = $request->name;
                $gv->ngaysinh = $request->date_birth;
                $gv->diachi = $request->address;
                $gv->gioitinh = $request->gender;
                $gv->email = $request->email;
                $gv->sdt = $request->phone;
                $gv->idtk = $tk->idtk;
                $gv->save();
            }
            if($request->role == 3){
                $tk = new Taikhoan;
                $tk->tentk = $request->username;
                $tk->matkhau = md5($request->password);
                $tk->save();
                $sv = new Sinhvien;
                $sv->hoten = $request->name;
                $sv->ngaysinh = $request->date_birth;
                $sv->gioitinh = $request->gender;
                $sv->diachi = $request->address;
                $sv->email = $request->email;
                $sv->sdt = $request->phone;
                $sv->idtk = $tk->idtk;
                $sv->save();
            }
            echo 'ok';
        }
    }
    public function get_sua($id){
        $admin = Admin::where('idtk',$id)->get();
        $gv = Giaovien::where('idtk',$id)->get();
        $sv = Sinhvien::where('idtk',$id)->get();
        if($admin->count() > 0){
            return view('nguoi_dung.sua',['data'=>$admin->first(),'role'=> 1]);
        } elseif ($gv->count() > 0) {
            return view('nguoi_dung.sua',['data'=>$gv->first(),'role'=> 2]);
        } else {
            return view('nguoi_dung.sua',['data'=>$sv->first(),'role'=> 3]);
        }
    }
    public function post_sua($id,Request $request){
        if($request->role == 1){
            $ad = Admin::where('idtk',$id)->first();
            $ad->hoten = $request->name;
            $ad->ngaysinh = $request->date_birth;
            $ad->diachi = $request->address;
            $ad->gioitinh = $request->gender;
            $ad->email = $request->email;
            $ad->sdt = $request->phone;
            $ad->save();
        }
        if($request->role == 2){
            $gv = Giaovien::where('idtk',$id)->first();
            $gv->hoten = $request->name;
            $gv->ngaysinh = $request->date_birth;
            $gv->diachi = $request->address;
            $gv->gioitinh = $request->gender;
            $gv->email = $request->email;
            $gv->sdt = $request->phone;
            $gv->save();
        }
        if($request->role == 3){
            $sv = Sinhvien::where('idtk',$id)->first();
            $sv->hoten = $request->name;
            $sv->ngaysinh = $request->date_birth;
            $sv->diachi = $request->address;
            $sv->gioitinh = $request->gender;
            $sv->email = $request->email;
            $sv->sdt = $request->phone;
            $sv->save();
        }
        echo "ok";
    }
    public function get_xoa($id)
    {
        $admin = Admin::where('idtk',$id)->get();
        $gv = Giaovien::where('idtk',$id)->get();
        $sv = Sinhvien::where('idtk',$id)->get();
        if($admin->count() > 0){
            $tk = Taikhoan::find($admin->first()->idtk)->delete();
            $admin->first()->delete();
        } elseif ($gv->count() > 0) {
            $check = Lop::where('idgv',$gv->first()->idgv)->count();
            if($check == 0){
            $tk = Taikhoan::find($gv->first()->idtk)->delete();
            $gv->first()->delete();
            } else {
                return back()->with('noti','Giáo viên này đang có lớp, không thể xóa');
            }
        } else {
            $tk = Taikhoan::find($sv->first()->idtk)->delete();
            $sv->first()->delete();
            $lsv = Lopsinhvien::where('idsv',$sv->first()->idsv)->delete();
        }
        return back()->with('noti','Xóa thành công');
    }
    public function get_ttcn()
    {
        $idtk = session()->get('id_user');
        $role = session()->get('role');
        if($role == 'admin'){
            $ng = Admin::where('idtk',$idtk)->first();
            return view('nguoi_dung.ttcn',['ng'=>$ng]);
        } elseif ($role == 'gv') {
            $ng = Giaovien::where('idtk',$idtk)->first();
            return view('nguoi_dung.ttcn',['ng'=>$ng]);
        } else {
            $ng = Sinhvien::where('idtk',$idtk)->first();
            return view('nguoi_dung.ttcn',['ng'=>$ng]);
        }
    }
    public function post_ttcn(Request $request)
    {
        $idtk = session()->get('id_user');
        $role = session()->get('role');
        if($role == 'admin'){
            $ng = Admin::where('idtk',$idtk)->first();
            $ng->hoten = $request->hoten;
            $ng->ngaysinh = $request->ngaysinh;
            $ng->email = $request->email;
            $ng->sdt = $request->sdt;
            $ng->diachi = $request->diachi;
            $ng->gioitinh = $request->gt;
            $ng->save();
        } elseif ($role == 'gv') {
            $ng = Giaovien::where('idtk',$idtk)->first();
            $ng->hoten = $request->hoten;
            $ng->ngaysinh = $request->ngaysinh;
            $ng->email = $request->email;
            $ng->sdt = $request->sdt;
            $ng->diachi = $request->diachi;
            $ng->gioitinh = $request->gt;
            $ng->save();
        } else {
            $ng = Sinhvien::where('idtk',$idtk)->first();
            $ng->hoten = $request->hoten;
            $ng->ngaysinh = $request->ngaysinh;
            $ng->email = $request->email;
            $ng->sdt = $request->sdt;
            $ng->diachi = $request->diachi;
            $ng->gioitinh = $request->gt;
            $ng->save();
        }
        return back()->with('noti','Sửa thành công');
    }
    public function get_doimk()
    {
        $idtk = session()->get('id_user');
        $role = session()->get('role');
        if($role == 'admin'){
            $ng = Admin::where('idtk',$idtk)->first();
            return view('nguoi_dung.doimk',['ng'=>$ng]);
        } elseif ($role == 'gv') {
            $ng = Giaovien::where('idtk',$idtk)->first();
            return view('nguoi_dung.doimk',['ng'=>$ng]);
        } else {
            $ng = Sinhvien::where('idtk',$idtk)->first();
            return view('nguoi_dung.doimk',['ng'=>$ng]);
        }
    }
    public function post_doimk(Request $request)
    {
        $idtk = session()->get('id_user');
        $ng = Taikhoan::find($idtk);
        if($ng->matkhau == md5($request->mk)){
            $ng->matkhau = md5($request->mkmoi);
            $ng->save();
            echo 'ok';
        } else {
            echo 'err';
        }
    }
}
