<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lop;
use App\Sinhvien;
use App\Giaovien;
use App\Baihoc;
use App\Binhluan;
use App\Lopsinhvien;
use App\Tailieu;
use App\Danhgia;

class TimkiemController extends Controller
{
    public function timkiem(Request $request)
    {
        $svtk = Sinhvien::where('hoten','like','%'.$request->tk.'%')->get();
        $bh = Baihoc::where('tenbaihoc','like','%'.$request->tk.'%')->get();
        $role = session()->get('role');
        if($role == 'admin'){
            $lop = Lop::where('tenlop','like','%'.$request->tk.'%')->leftjoin('giaovien','lop.idgv','=','giaovien.idgv')->select('lop.*','giaovien.hoten')->latest()->get();
            $data = array();
            foreach($lop as $lp){
                $sv = Lopsinhvien::where('idlop',$lp->idlop)->count();
                $bai = Baihoc::where('idlop',$lp->idlop)->count();
                $bl = Binhluan::where('idlop',$lp->idlop)->count();
                $dg = Danhgia::where('idlop',$lp->idlop)->avg('danhgia');
                $data[] = ['id'=>$lp->idlop,'ten'=>$lp->tenlop,'gv'=>$lp->hoten,'sv'=>$sv,'bai'=>$bai,'bl'=>$bl,'danhgia'=>$dg];
            }
            return view('timkiem',['data'=>$data,'bai'=>$bh,'sv'=>$svtk]);
        } elseif($role == 'gv'){
            $idgv = Giaovien::where('idtk',session()->get('id_user'))->first();
            $lop = Lop::where('lop.idgv',$idgv->idgv)->where('tenlop','like','%'.$request->tk.'%')->leftjoin('giaovien','lop.idgv','=','giaovien.idgv')->select('lop.*','giaovien.hoten')->latest()->get();
            $data = array();
            foreach($lop as $lp){
                $sv = Lopsinhvien::where('idlop',$lp->idlop)->count();
                $bai = Baihoc::where('idlop',$lp->idlop)->count();
                $bl = Binhluan::where('idlop',$lp->idlop)->count();
                $dg = Danhgia::where('idlop',$lp->idlop)->avg('danhgia');
                $data[] = ['id'=>$lp->idlop,'ten'=>$lp->tenlop,'gv'=>$lp->hoten,'sv'=>$sv,'bai'=>$bai,'bl'=>$bl,'danhgia'=>$dg];
            }
            return view('timkiem',['data'=>$data,'bai'=>$bh,'sv'=>$svtk]);
        } else {
            $idsv = Sinhvien::where('idtk',session()->get('id_user'))->first();
            $lsv = Lopsinhvien::where('idsv',$idsv->idsv)->get();
            $arr = array();
            foreach($lsv as $l){
                $arr[] = $l->idlop;
            }
            $lop = Lop::whereIn('idlop',$arr)->where('tenlop','like','%'.$request->tk.'%')->leftjoin('giaovien','lop.idgv','=','giaovien.idgv')->select('lop.*','giaovien.hoten')->latest()->get();
            $data = array();
            foreach($lop as $lp){
                $sv = Lopsinhvien::where('idlop',$lp->idlop)->count();
                $bai = Baihoc::where('idlop',$lp->idlop)->count();
                $bl = Binhluan::where('idlop',$lp->idlop)->count();
                $dg = Danhgia::where('idlop',$lp->idlop)->avg('danhgia');
                $data[] = ['id'=>$lp->idlop,'ten'=>$lp->tenlop,'gv'=>$lp->hoten,'sv'=>$sv,'bai'=>$bai,'bl'=>$bl,'danhgia'=>$dg];
            }
            return view('timkiem',['data'=>$data,'bai'=>$bh,'sv'=>$svtk]);
        }
    }
}
