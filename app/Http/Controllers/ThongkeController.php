<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Sinhvien;
use App\Giaovien;
use App\Lop;
use App\Baihoc;
use App\Tailieu;

class ThongkeController extends Controller
{
    public function thongke()
    {
        $sv = Sinhvien::all()->count();
        $gv = Giaovien::all()->count();
        $lop = Lop::all()->count();
        $bh = Baihoc::all()->count();
        $bhkt = Baihoc::whereDate('ngayketthuc','<',Carbon::now()->toDateString())->count();
        $tl = Tailieu::all()->count();
        return view('thongke.thongke',['sv'=>$sv,'gv'=>$gv,'lop'=>$lop,'bh'=>$bh,'bhkt'=>$bhkt,'tl'=>$tl]);
    }
}
