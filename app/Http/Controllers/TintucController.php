<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use App\Tintuc;

class TintucController extends Controller
{
    public function ds(){
        $tt = Tintuc::latest()->get();
        return view('tintuc.ds',['tintuc'=>$tt]);
    }
    public function get_them(){
        return view('tintuc.them');
    }
    public function post_them(Request $request)
    {
        $tt = new Tintuc;
        $tt->tieude = $request->tieude;
        $tt->noidung = $request->noidung;
        if($request->hasFile('hinhnho')){
            $file = $request->file('hinhnho');
            $name = Str::random(5).'_'.$file->getClientOriginalName(); //Lấy tên file
            $destination = base_path() . '/public/images';
            $file->move($destination, $name);
            $tt->hinhnho = $name;
        }
        $tt->save();
        return back()->with('noti','Thêm thành công');
    }
    public function get_sua($idtt)
    {
        $tt = Tintuc::find($idtt);
        return view('tintuc.sua',['tintuc'=>$tt]);
    }
    public function post_sua($idtt, Request $request)
    {
        $tt = Tintuc::find($idtt);
        $tt->tieude = $request->tieude;
        $tt->noidung = $request->noidung;
        if($request->hasFile('hinhnho')){
            $path = public_path().'/images/'.$tt->hinhnho;
            if(File::exists($path)){
                File::delete($path);
            }
            $file = $request->file('hinhnho');
            $name = Str::random(5).'_'.$file->getClientOriginalName(); //Lấy tên file
            $destination = base_path() . '/public/images';
            $file->move($destination, $name);
            $tt->hinhnho = $name;
        }
        $tt->save();
        return back()->with('noti','Sửa thành công');
    }
    public function xoa($idtt)
    {
        $tt = Tintuc::find($idtt);
        if(!empty($tt->hinhnho)){
            $path = public_path().'/images/'.$tt->hinhnho;
            if(File::exists($path)){
                File::delete($path);
            }
        }
        $tt->delete();
        return back()->with('noti','Xóa thành công');
    }
    public function xem($idtt)
    {
        $tt = Tintuc::find($idtt);
        return view('tintuc.xem',['tintuc'=>$tt]);
    }
}
