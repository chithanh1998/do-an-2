<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Lop;
use App\Sinhvien;
use App\Giaovien;
use App\Baihoc;
use App\Binhluan;
use App\Lopsinhvien;
use App\Tailieu;
use App\Danhgia;
use App\Nopbaitap;

class LopController extends Controller
{
    public function ds(){
        $gv = Giaovien::all();
        $role = session()->get('role');
        if($role == 'admin'){
            $lop = Lop::leftjoin('giaovien','lop.idgv','=','giaovien.idgv')->select('lop.*','giaovien.hoten')->latest()->get();
            $data = array();
            foreach($lop as $lp){
                $sv = Lopsinhvien::where('idlop',$lp->idlop)->count();
                $bai = Baihoc::where('idlop',$lp->idlop)->count();
                $bl = Binhluan::where('idlop',$lp->idlop)->count();
                $dg = Danhgia::where('idlop',$lp->idlop)->avg('danhgia');
                $data[] = ['id'=>$lp->idlop,'ten'=>$lp->tenlop,'gv'=>$lp->hoten,'sv'=>$sv,'bai'=>$bai,'bl'=>$bl,'danhgia'=>$dg];
            }
            return view('lop.ds_lop',['data'=>$data,'dsgv'=>$gv]);
        } elseif($role == 'gv'){
            $idgv = Giaovien::where('idtk',session()->get('id_user'))->first();
            $lop = Lop::where('lop.idgv',$idgv->idgv)->leftjoin('giaovien','lop.idgv','=','giaovien.idgv')->select('lop.*','giaovien.hoten')->latest()->get();
            $data = array();
            foreach($lop as $lp){
                $sv = Lopsinhvien::where('idlop',$lp->idlop)->count();
                $bai = Baihoc::where('idlop',$lp->idlop)->count();
                $bl = Binhluan::where('idlop',$lp->idlop)->count();
                $dg = Danhgia::where('idlop',$lp->idlop)->avg('danhgia');
                $data[] = ['id'=>$lp->idlop,'ten'=>$lp->tenlop,'gv'=>$lp->hoten,'sv'=>$sv,'bai'=>$bai,'bl'=>$bl,'danhgia'=>$dg];
            }
            return view('lop.ds_lop',['data'=>$data,'dsgv'=>$gv]);
        } else {
            $idsv = Sinhvien::where('idtk',session()->get('id_user'))->first();
            $lsv = Lopsinhvien::where('idsv',$idsv->idsv)->get();
            $arr = array();
            foreach($lsv as $l){
                $arr[] = $l->idlop;
            }
            $lop = Lop::whereIn('idlop',$arr)->leftjoin('giaovien','lop.idgv','=','giaovien.idgv')->select('lop.*','giaovien.hoten')->latest()->get();
            $data = array();
            foreach($lop as $lp){
                $sv = Lopsinhvien::where('idlop',$lp->idlop)->count();
                $bai = Baihoc::where('idlop',$lp->idlop)->count();
                $bl = Binhluan::where('idlop',$lp->idlop)->count();
                $dg = Danhgia::where('idlop',$lp->idlop)->avg('danhgia');
                $data[] = ['id'=>$lp->idlop,'ten'=>$lp->tenlop,'gv'=>$lp->hoten,'sv'=>$sv,'bai'=>$bai,'bl'=>$bl,'danhgia'=>$dg];
            }
            return view('lop.ds_lop',['data'=>$data,'dsgv'=>$gv]);
        }
    }
    public function themlop(Request $request){
        $check = Lop::where('tenlop',$request->tenlop)->count();
        if($check == 0){
        $lop = new Lop;
        $lop->tenlop = $request->tenlop;
        $lop->idgv = $request->idgv;
        $lop->save();
        echo "ok";
        } else {
            echo "err";
        }
    }
    public function xoalop($id_lop)
    {
        $lop = Lop::find($id_lop)->delete();
        $bh = Baihoc::where('idlop',$id_lop)->delete();
        $lsv = Lopsinhvien::where('idlop',$id_lop)->delete();
        return back()->with('noti','Xóa thành công');
    }
    public function get_sualop(Request $request,$id){
        $lop = Lop::where('idlop',$id)->first();
        echo json_encode($lop);
    }
    public function post_sualop(Request $request,$id){
        $check = Lop::where('tenlop',$request->tenlop)->count();
        if($check != 0){
            echo 'err';
        } else {
        $lop = Lop::where('idlop',$id)->first();
        $lop->tenlop = $request->tenlop;
        $lop->idgv = $request->idgv;
        $lop->save();
        echo 'ok';
        }
    }
    public function dssv($id){
        $sv = Lopsinhvien::where('idlop',$id)->leftjoin('sinhvien','lop-sinhvien.idsv','=','sinhvien.idsv')->get();
        $dssv = array();
        foreach($sv as $s){
            $dssv[] = $s->idsv;
        }
        $ds = Sinhvien::whereNotIn('idsv',$dssv)->get();
        $lop = Lop::find($id);
        return view('lop.ds_sinhvien',['sv'=>$sv,'dssv'=>$ds,'lop'=>$lop]);
    }
    public function xoasv($id_lop,$id_sv){
        $sv = Lopsinhvien::where('idlop',$id_lop)->where('idsv',$id_sv)->delete();
        return redirect('lop/'.$id_lop.'/sinhvien')->with('noti','Xóa thành công');
    }
    public function themsv($id_lop, Request $request){
        $sv = new Lopsinhvien;
        $sv->idlop = $id_lop;
        $sv->idsv = $request->sv;
        $sv->save();
        echo 'ok';
    }
    public function dsbaihoc($id_lop){
        $bai = Baihoc::where('idlop',$id_lop)->get();
        $lop = Lop::find($id_lop);
        return view('lop.ds_baihoc',['bai'=>$bai,'lop'=>$lop]);
    }
    public function get_thembaihoc($id_lop){
        $lop = Lop::find($id_lop);
        return view('lop.them_baihoc',['lop'=>$lop]);
    }
    public function post_thembaihoc($id_lop, Request $request){
        $bai = new Baihoc;
        $bai->tenbaihoc = $request->tenbaihoc;
        $bai->noidung = $request->noidung;
        $bai->ngaytao = $request->ngaytao;
        $bai->ngayketthuc = $request->ngayketthuc;
        $bai->idlop = $id_lop;
        $bai->save();
        if($request->hasFile('tailieu')){
            $file = $request->file('tailieu');
            $name = $file->getClientOriginalName(); //Lấy tên file
            $destination = base_path() . '/public/tailieu';
            $file->move($destination, $name);
            $tl = new Tailieu;
            $tl->duongdan = $name;
            $tl->idbh = $bai->idbh;
            $tl->save();
        }
        return back()->with('noti','Thêm thành công');
    }
    public function get_suabaihoc($id_lop,$id_bh){
        $lop = Lop::find($id_lop);
        $bh = Baihoc::find($id_bh);
        $tl = Tailieu::where('idbh',$id_bh)->get();
        return view('lop.sua_baihoc',['lop'=>$lop,'bh'=>$bh,'tailieu'=>$tl]);
    }
    public function post_suabaihoc($id_lop,$id_bh, Request $request){
        $bh = Baihoc::find($id_bh);
        $bh->tenbaihoc = $request->tenbaihoc;
        $bh->noidung = $request->noidung;
        $bh->ngaytao = $request->ngaytao;
        $bh->ngayketthuc = $request->ngayketthuc;
        $bh->save();
        if($request->hasFile('tailieu')){
            $file = $request->file('tailieu');
            $name = $file->getClientOriginalName(); //Lấy tên file
            $destination = base_path() . '/public/tailieu';
            $file->move($destination, $name);
            $tl = new Tailieu;
            $tl->duongdan = $name;
            $tl->idbh = $bh->idbh;
            $tl->save();
        }
        return back()->with('noti','Sửa thành công');
    }
    public function xoatailieu($id_tl){
        $tl = Tailieu::find($id_tl);
        $path = public_path().'/tailieu/'.$tl->duongdan;
        if(File::exists($path)){
            File::delete($path);
        }
        $tl->delete();
        return back()->with('noti','Xóa thành công');
    }
    public function xoabaihoc($id_lop,$id_bh){
        $bh = Baihoc::where('idbh',$id_bh)->delete();
        $tl = Tailieu::where('idbh',$id_bh)->get();
        foreach($tl as $t){
            $path = public_path().'/tailieu/'.$t->duongdan;
            if(File::exists($path)){
                File::delete($path);
            }
        }
        $xtl = Tailieu::where('idbh',$id_bh)->delete();
        return back()->with('noti',"Xóa thành công");
    }
    public function xembaihoc($id_lop,$id_bh){
        $lop = Lop::find($id_lop);
        $bh = Baihoc::find($id_bh);
        $tl = Tailieu::where('idbh',$id_bh)->get();
        return view('lop.xem_baihoc',['lop'=>$lop,'bh'=>$bh,'tailieu'=>$tl]);
    }
    public function dsbinhluan($id_lop){
        $bl = Binhluan::where('idlop',$id_lop)->leftjoin('taikhoan','binhluan.idtk','=','taikhoan.idtk')->select('binhluan.*','taikhoan.tentk')->latest()->get();
        $lop = Lop::find($id_lop);
        return view('lop.binhluan',['lop'=>$lop,'binhluan'=>$bl]);
    }
    public function thembinhluan($id_lop, Request $request){
        $bl = new Binhluan;
        $bl->idtk = session()->get('id_user');
        $bl->idlop = $id_lop;
        $bl->noidung = $request->noidung;
        $bl->save();
        echo 'ok';
    }
    public function xoabinhluan($id_bl){
        $bl = Binhluan::find($id_bl)->delete();
        return back()->with('noti','Xóa thành công');
    }
    public function layid(Request $request)
    {
        return $request->id;
    } public function danhgia(Request $request)
    {
        $check = Danhgia::where('idtk',session()->get('id_user'))->where('idlop',$request->idlop)->count();
        if($check != 0){
            echo 'err';
        } else {
            $dg = new Danhgia;
            $dg->idlop = $request->idlop;
            $dg->idtk = session()->get('id_user');
            $dg->danhgia = $request->danhgia;
            $dg->save();
            echo 'ok';
        }
    }
    public function dsnopbaitap($id_lop,$id_bh) 
    {
        $ds = Nopbaitap::where('idbh',$id_bh)->leftjoin('sinhvien','nopbaitap.idsv','=','sinhvien.idsv')->select('nopbaitap.*','sinhvien.hoten')->get();
        $lop = Lop::find($id_lop);
        return view('lop.ds_baitap',['lop'=>$lop,'ds'=>$ds]);
    }
    public function get_nopbaitap($id_lop,$id_bh)
    {
        $lop = Lop::find($id_lop);
        $idsv = session()->get('idsv');
        $bt = Nopbaitap::where('idbh',$id_bh)->where('idsv',$idsv)->get();
        return view('lop.nop_baitap',['lop'=>$lop,'id_bh'=>$id_bh,'dsbt'=>$bt]);
    }
    public function post_nopbaitap($id_lop,$id_bh, Request $request)
    {
        $idsv = session()->get('idsv');
        $check = Nopbaitap::where('idbh',$id_bh)->where('idsv',$idsv)->count();
        if($check > 0){
            return back()->with('err','Bạn đã nôp bài tập rồi');;
        } else {
            $bt = new Nopbaitap;
            $bt->tieude = $request->tieude;
            $bt->idbh = $id_bh;
            $bt->idsv = $idsv;
            if($request->hasFile('tailieu')){
                $file = $request->file('tailieu');
                $name = $file->getClientOriginalName(); //Lấy tên file
                $destination = base_path() . '/public/upload';
                $file->move($destination, $name);
                $bt->file = $name;
                $bt->save();
            } else {
                return back()->with('err','Bạn chưa chọn file');;
            }
            return back()->with('noti','Nộp thành công');;
        }
    }
    public function xoabaitap($id_bt)
    {
        $bt = Nopbaitap::find($id_bt);
        $path = public_path().'/upload/'.$bt->file;
        if(File::exists($path)){
            File::delete($path);
        }
        $bt->delete();
        return back()->with('noti','Xóa thành công');;
    }
}
