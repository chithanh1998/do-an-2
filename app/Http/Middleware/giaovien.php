<?php

namespace App\Http\Middleware;

use Closure;

class giaovien
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('role') == 'admin' || session('role') == 'gv'){
            return $next($request);
        }
        return back()->with('noti','Tài khoản bạn không đủ quyền');    }
}
