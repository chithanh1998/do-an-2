<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tailieu extends Model
{
    protected $table = 'tailieu';
    protected $primaryKey = 'idtl';
}
