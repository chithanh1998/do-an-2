<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Danhgia extends Model
{
    protected $table = 'danhgia';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
