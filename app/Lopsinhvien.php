<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lopsinhvien extends Model
{
    protected $table = 'lop-sinhvien';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
