<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Baihoc extends Model
{
    protected $table = 'baihoc';
    protected $primaryKey = 'idbh';
    public $timestamps = false;
}
